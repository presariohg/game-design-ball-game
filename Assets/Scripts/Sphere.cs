using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Sphere : MonoBehaviour {

    // public string cupointTag = "Cupoints";

    private Rigidbody rb;
    private Vector3 startPosition;
    private float boundCheckInterval = 0.2f;
    private float boundCheckCountdown = 0;
    private AudioSource bgm;

    public Vector3 boundary = new Vector3(20, 5, 20);
    public float dragForce = 1f;
    public AudioSource dieSound;
    public AudioClip bumpSound;
    public AudioClip coinSound;

    // Start is called before the first frame update
    void Start() {
        startPosition = transform.position;
        startPosition.y += 3;
        rb = gameObject.GetComponent<Rigidbody>();
        rb.drag = dragForce;
        bgm = Camera.main.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.W))
            rb.AddForce(Vector3.forward);

        if (Input.GetKey(KeyCode.A))
            rb.AddForce(Vector3.left);

        if (Input.GetKey(KeyCode.S))
            rb.AddForce(Vector3.back);

        if (Input.GetKey(KeyCode.D))
            rb.AddForce(Vector3.right);

        if (boundCheckCountdown <= 0) {
            boundCheckCountdown = boundCheckInterval;
            StartCoroutine(CheckBound());
        }
        boundCheckCountdown -= Time.deltaTime;
    }


    void OnCollisionEnter(Collision collision) {
        if (collision.relativeVelocity.magnitude > 1) {
            bgm.PlayOneShot(bumpSound, 1.5f);  //1.5x bgm volume
        }
    }

    public void HitCupoint() {
        bgm.PlayOneShot(coinSound, 1.5f);
    }


    // Reset the ball's position if shot out of the stage
    IEnumerator CheckBound() {
        Vector3 currentPosition = transform.position;
        if ((Mathf.Abs(currentPosition.x) >= boundary.x) ||
            (Mathf.Abs(currentPosition.y) >= boundary.y) ||
            (Mathf.Abs(currentPosition.z) >= boundary.z)) {

            // Pause the game, play die sound, restart
            Camera.main.GetComponent<AudioSource>().Pause();

            if (!dieSound.isPlaying)
                dieSound.Play();

            while (dieSound.isPlaying)
                yield return null;

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        yield return new WaitForSeconds(0.2f);
    }
}
