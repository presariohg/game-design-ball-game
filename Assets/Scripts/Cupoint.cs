using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cupoint : MonoBehaviour {
    public float hoverLimitY = 0.2f;
    public float hoverSpeedY = 5f;
    public float angularVelocity = 75;

    void Start() {
    }

    // Update is called once per frame
    void Update() {
        transform.RotateAround(transform.position, Vector3.up, angularVelocity * Time.deltaTime);

        // Hover on Y axis
        float newY = Mathf.Sin(Time.time * hoverSpeedY) * hoverLimitY;
        Vector3 currentPosition = transform.position;
        Vector3 newPosition = new Vector3(currentPosition.x, newY, currentPosition.z);

        Vector3 movingDirection = newPosition - currentPosition;
        transform.Translate(movingDirection, Space.World);
    }

    void OnTriggerEnter(Collider collider) {
        collider.gameObject.GetComponent<Sphere>().HitCupoint();
        Destroy(gameObject);
    }
} 